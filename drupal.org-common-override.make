;; (non-private) Drupal.org Theme
projects[bluecheese][type] = "theme"
projects[bluecheese][download][type] = "git"
projects[bluecheese][download][url] = "https://git.drupalcode.org/project/bluecheese.git"
projects[bluecheese][download][branch] = "7.x-2.x"
