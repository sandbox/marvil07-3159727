# A minimal lando setup for drupal.org

This is small set of config to get the set of drupal.org codebase locally via
[lando](https://lando.dev/).

## Getting started

One way to use this is the following

    # Clone the git respository.
    git clone https://git.drupalcode.org/sandbox/marvil07-3159727.git drupalorg_lando
    cd drupalorg_lando
    # Start lando application.
    lando start
    # The first time, setup the codebase.
    # This is just skipping private bluecheese repository.
    lando drush make --overrides="drupal.org-common-override.make" https://bitbucket.org/drupalorg-infrastructure/drupal.org/raw/c7fe3c5696e62bfc410d3bbd73a3506880568a61/drupal.org.make web

After that you can go to https://drupalorg.lndo.site with the codebase at `web/`
directory.

@todo Database setup or install via a profile?
